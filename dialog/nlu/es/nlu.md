## intent:test_numbers_dates
`You can also reuse intents from other skills. Use this syntax then:` \
`- I would like to donate you [one thousand](skill_dialogs-numbers2thousand) euros` \
`The entity name follows "skill_name-the_entity" style, with a skill name of "Skill-Name" and an entity name of "the_entity".` \
`It also has no ".txt" ending in this case (they are only used to make links in the skill repository clickable).`
- ¿Puedes decir [veintiuno](skill_dialogs-numbers2hundred) acertijos [mañana](skill_dialogs-date)?

## intent:test_roles
`To add two entities of the same type to a single intent, you can give them roles like this:`
- ¿Cuánto es [uno](skill_dialogs-numbers2hundred?number1) más [dos](skill_dialogs-numbers2hundred?number2)?
