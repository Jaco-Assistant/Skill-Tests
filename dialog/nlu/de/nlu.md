## intent:test_numbers_dates
`You can also reuse intents from other skills. Use this syntax then:` \
`- I would like to donate you [one thousand](skill_dialogs-numbers2thousand) euros` \
`The entity name follows "skill_name-the_entity" style, with a skill name of "Skill-Name" and an entity name of "the_entity".` \
`It also has no ".txt" ending in this case (they are only used to make links in the skill repository clickable).`
- Kannst du mir [morgen](skill_dialogs-date) [einundzwanzig](skill_dialogs-numbers2hundred) Rätsel erzählen?

## intent:test_roles
`To add two entities of the same type to a single intent, you can give them roles like this:`
- Was ist [eins](skill_dialogs-numbers2hundred?number1) plus [zwei](skill_dialogs-numbers2hundred?number2)?
