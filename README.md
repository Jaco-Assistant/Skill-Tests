# Skill-Tests

A skill to test various dialog options with _Jaco_.

[![pipeline status](https://gitlab.com/Jaco-Assistant/Skill-Tests/badges/master/pipeline.svg)](https://gitlab.com/Jaco-Assistant/Skill-Tests/-/commits/master)
[![code style black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![code complexity](https://gitlab.com/Jaco-Assistant/Skill-Tests/-/jobs/artifacts/master/raw/badges/rcc.svg?job=analysis)](https://gitlab.com/Jaco-Assistant/Skill-Tests/-/commits/master)
