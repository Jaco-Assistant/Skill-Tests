import os

from jacolib import assistant

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
assist: assistant.Assistant


# ==================================================================================================


def callback_test_numbers_dates(message):
    """When triggered skills asks a 'yes/no' question to test questions as well, and responds with
    'no' if 'yes' was chosen and doesn't respond otherwise"""

    qi = ["skill_dialogs-confirm", "skill_dialogs-refuse"]
    question_sentence = assist.get_random_talk("yes_no")

    answer = assist.publish_question(
        question_sentence, question_intents=qi, satellite=message["satellite"]
    )

    if answer != {}:
        if answer["intent"]["name"] == "skill_dialogs-confirm":
            result_sentence = assist.get_random_talk("no")
            assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_test_roles(message):
    solutions = assist.extract_entities(
        message, "skill_dialogs-numbers2hundred", values_only=False
    )
    print(solutions)

    if len(solutions) == 2:
        num1 = int([sol for sol in solutions if sol["role"] == "number1"][0]["value"])
        num2 = int([sol for sol in solutions if sol["role"] == "number2"][0]["value"])
        nsum = num1 + num2
        result_sentence = str(nsum)
    else:
        result_sentence = assist.get_random_talk("no")
    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def main():
    global assist

    assist = assistant.Assistant(repo_path=filepath)

    assist.add_topic_callback("test_roles", callback_test_roles)
    assist.add_topic_callback("test_numbers_dates", callback_test_numbers_dates)
    assist.run()


# ==================================================================================================

if __name__ == "__main__":
    main()
